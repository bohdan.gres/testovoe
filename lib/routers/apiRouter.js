const express = require('express');
const controllers = require('./../controllers');
const jwtChecker = require('./../middlewares/JWTcheck');

const router = express.Router();

router.post('/signin', controllers.session.create());

router.post('/signup', controllers.users.create());

router.route('/chats')
    .get(jwtChecker, controllers.chats.list())
    .post(jwtChecker, controllers.chats.create());

router.route('/messages')
    .post(jwtChecker, controllers.messages.create());

router.route('/messages/:id')
    .get(jwtChecker, controllers.messages.show())
    .put(jwtChecker, controllers.messages.update());

module.exports = router;




// Сделать сервис с REST API. Авторизация по токену (/chat, /message).  Настроенный CORS для доступа с любого домена. DB - MongoDB. Токен создавать при каждом заходе, действителен 10 минут. Продлевать при любом запросе пользователя (кроме signin и signup).
//
// API:
//     • /signin [POST] - запрос токена по email и паролю // данные принимает в json
//  • /signup [POST] - регистрация нового пользователя: // данные принимает в json
//  • При удачной регистрации или логинации вернуть токен.
//  • Пароль храниться в формате hex
//  • /chat [GET] - возвращает список чатов для текущего пользователя с сообщениями
//  • /chat [POST] - создает чат с определённым пользователем // передается id пользователя
//  • /message [POST] - создание сообщения в чате
//  • /message/:id [GET] - возвращяет сообщение
//  • /message/:id [PUT] - обновляет сообщение
//
// Использовать express для маршрутизации. Mongoose для БД.
