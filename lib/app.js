const express = require('express');
const { port } = require('./../etc/config');
const loaders = require('./loaders');

module.exports = async () => {
    const app = express();

    await loaders({ app });

    app.listen(port, error => {
        if (error) {
            console.log(error);
            return;
        }
        console.log(`Server is listening on ${port}`);
    });

};
