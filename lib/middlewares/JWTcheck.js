const jwt = require('jsonwebtoken');
const { jwtSecret } = require('./../../etc/config');

module.exports = (req, res, next) => {
    jwt.verify(req.headers.jwt, jwtSecret, (err, decoded) => {
      if (err) {
          res.status(403).json({
              message: 'Failed to authenticate token.'
          });
      } else {
          req.session = {
              context: decoded.userData
          };
          next()
      }
    });
};
