const mongoose = require('mongoose');
const uuid = require('uuid/v4');
const { Schema, Model } = mongoose;

const schema = new Schema(
    {
        id: {
            type: String,
            required: true,
            default: uuid
        },
        userId: {
            type: String,
            required: true,
        }
    }
);

class Chat extends Model {

}

module.exports = mongoose.model(Chat, schema);
