const userMap = (user) => ({
    email: user.email,
    id: user.id
});

const dumpMessage = (message) => ({
    id : message.id,
    userId: message.userId,
    chatId: message.chatId,
    message: message.message
});

module.exports = {
    userMap,
    dumpMessage
};
