const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const uuid = require('uuid/v4');
const { saltRounds } = require('../../etc/config');
const { Schema, Model } = mongoose;

const schema = new Schema(
    {
        id: {
            type: String,
            required: true,
            default: uuid
        },
        email: {
            type: String,
            required: true,
        },
        password: {
            type: String,
            required: true,
        }
    }
);

class User extends Model {
    checkPassword(plain) {
        return new Promise((resolve, reject) => {
            bcrypt.compare(plain, this.password, (error, result) => {
                if (error) reject(error);
                resolve(result);
            });
        });
    }

    static async encryptPassword(password) {
        const salt = await new Promise( (resolve, reject) => {
            bcrypt.genSalt(saltRounds, (error, result) => {
                if (error) reject(error);
                resolve(result);
            })
        });

        return new Promise( (resolve, reject) => {
            bcrypt.hash(password, salt, (error, result) => {
                if (error) reject(error);
                resolve({ password: result });
            });
        });
    }
}

module.exports = mongoose.model(User, schema);
