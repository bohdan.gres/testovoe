const expressWinston = require('express-winston');
const winston = require('winston');
const { errorLogsFilePath } = require('../../etc/config');

const logger = winston.createLogger({
    level: 'error',
    format: winston.format.json(),
    transports: [
        new winston.transports.File({
            name: 'error',
            filename: errorLogsFilePath,
        }),
    ]
});

if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
        format: winston.format.simple()
    }));
}

module.exports = logger;
