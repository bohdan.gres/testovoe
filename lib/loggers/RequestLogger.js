const expressWinston = require('express-winston');
const winston = require('winston');
const { logsFilePath } = require('../../etc/config');

module.exports = expressWinston.logger({
    transports: [
        new winston.transports.File({
            name: 'access',
            filename: logsFilePath,
            level: 'info'
        })
    ],
    colorStatus: true
});
