const mongoLoader = require('./mongoLoader');
const serverLoader = require('./serverLoader');

module.exports = async ({ app }) => {
    await mongoLoader();
    await serverLoader({ app });
};
