const bodyParser = require('body-parser');
const cors = require('cors');
const requestLoger = require('../loggers/RequestLogger');
const apiRouter = require('./../routers/apiRouter');

module.exports = async ({ app }) => {

    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(bodyParser.json());

    app.use(requestLoger);

    app.use('/api/v1', apiRouter);

};

