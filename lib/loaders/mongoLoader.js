const mongoose = require('mongoose');
const { mongooseUrl, dbName } = require('./../../etc/dbConfig');

module.exports = async () => {
    mongoose.Promise = Promise;
    await mongoose.connect(`${mongooseUrl}${dbName}`, { useNewUrlParser: true });
};
