const cloneDeep = require('lodash.clonedeep');
const logger = require('../loggers/ErrorLogger');

class Base {
    static makeServiceRunner(
        actionClass,
        paramsBuilder = req => cloneDeep(req.body),
        contextBuilder = req => cloneDeep(req.session && req.session.context ? req.session.context : {}),
    ) {
        return async function serviceRunner(req, res) {
            try {
                const result = await Base.runService(actionClass, {
                    params: paramsBuilder(req),
                    context: contextBuilder(req)
                });
                Base.ppResult(req, res, result);
            } catch (e) {
                Base.returnError(req, res, e);
            }
        };
    }

    static async runService(service, { context = {}, params = {} }) {
        let res;
        try {
            res = await new service(context).run(params);
        } catch (e) {
            throw e;
        }
        return res;
    }

    static ppResult(req, res, result) {
        res.status(200);
        res.send(result);
    }

    static returnError(req, res, error) {
        if (error.isCustomError) {
            res.status(error.statusCode);
            res.send({
                Status: 0,
                errors: error.message
            });
        } else {
            logger.log({
                level: 'error',
                message: error.stack
            });
            res.status(500);
            res.send({
                Status: 0,
                Message: 'Server ERROR',
                error
            });
        }
    }
}

module.exports = Base;
