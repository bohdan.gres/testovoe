const chats    = require('./Chats');
const messages = require('./Messages');
const session  = require('./Session');
const users    = require('./Users');

module.exports = {
    chats,
    messages,
    session,
    users
};
