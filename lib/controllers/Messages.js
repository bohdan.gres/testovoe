const BaseController = require('./BaseAuthorized');
const services = require('./../services');

class Chats extends BaseController {
    static create(req, res) {
        return super.makeServiceRunner(
            services.Messages.Create
        )
    }

    static show(req,res) {
        return super.makeServiceRunner(
            services.Messages.Show,
            req => ({ messageId: req.params.id })
        )
    }

    static update(req,res) {
        return super.makeServiceRunner(
            services.Messages.Update,
            req => ({ messageId: req.params.id, ...req.body })
        )
    }
}

module.exports = Chats;
