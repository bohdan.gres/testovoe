const BaseController = require('./BaseAuthorized');
const services = require('./../services');

class Chats extends BaseController {
    static create(req, res) {
        return super.makeServiceRunner(
            services.Chats.Create
        )
    }

    static list(req, res) {
        return super.makeServiceRunner(
            services.Chats.List
        )
    }
}

module.exports = Chats;
