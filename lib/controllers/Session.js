const BaseController = require('./Base');
const services = require('./../services');

class Session extends BaseController {
    static create(req, res) {
        return super.makeServiceRunner(
            services.Session.Create
        )
    }
}

module.exports = Session;
