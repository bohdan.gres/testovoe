const cloneDeep = require('lodash.clonedeep');
const jwt = require('jsonwebtoken');
const Base = require('./Base');
const { jwtSecret, jwtTime } = require('./../../etc/config');

class BaseAuthorized extends Base {
    static makeServiceRunner(
        actionClass,
        paramsBuilder = req => cloneDeep(req.body),
        contextBuilder = req => cloneDeep(req.session && req.session.context ? req.session.context : {}),
    ) {
        return async function serviceRunner(req, res) {
            try {
                const result = await Base.runService(actionClass, {
                    params: paramsBuilder(req),
                    context: contextBuilder(req)
                });
                BaseAuthorized.ppResult(req, res, result);
            } catch (e) {
                Base.returnError(req, res, e);
            }
        };
    }

    static ppResult(req, res, result) {
        res.status(200);
        res.send({
            ...result,
            jwt: jwt.sign({ userData: { ...req.session.context }}, jwtSecret, { expiresIn: jwtTime }),
        });
    }
}

module.exports = BaseAuthorized;
