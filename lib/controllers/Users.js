const BaseController = require('./Base');
const services = require('./../services');

class Users extends BaseController {
    static create(req, res) {

        return super.makeServiceRunner (
            services.Users.Create
        );
    }
}

module.exports = Users;
