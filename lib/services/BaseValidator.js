const Validator = require("fastest-validator");

class BaseValidator {
    constructor(schema) {
        this.validator = new Validator();
        this.validateRuner = this.validator.compile(schema);
    }

    validate(params) {
        return this.validateRuner(params);
    }
}

module.exports = BaseValidator;
