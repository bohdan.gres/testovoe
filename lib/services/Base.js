const ValidattionError = require('../errors/ValidationError');
class Base {
    constructor(context) {
        this.context = context ? context : {};
    }

    async run(params) {
        const clearParams = this.validate(params);

        return  this.execute(clearParams);
    }

    validate(params) {
        const validattionResult = this.validator.validate(params);
        if (validattionResult !== true) {
            throw new ValidattionError({ message: validattionResult });
        }
        return params;
    }

}

module.exports = Base;
