const BaseValidator = require('./../../BaseValidator');

const schema = {
    id: { type: "number", positive: true, integer: true },
    name: { type: "string", min: 3, max: 255 },
    status: "boolean" // short-hand def
};

class DeleteValidator extends BaseValidator {
    constructor() {
        super(schema);
    }
}

module.exports = new CreateVa
