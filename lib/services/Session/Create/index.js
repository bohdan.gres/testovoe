const jwt = require('jsonwebtoken');
const { jwtSecret, jwtTime } = require('./../../../../etc/config');
const Base = require('./../../Base');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const BaseError = require('./../../../errors/BaseError');
const { userMap } = require('./../../../models/utils');

class Create extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ email, password }) {
        const existedUser = await User.findOne({ email });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }

        const passwordCheck = await existedUser.checkPassword(password);

        if (!passwordCheck) {
            throw new BaseError({ message: 'Wrong password' });
        }

        return {
            Status : 1,
            jwt: jwt.sign({ userData : { email: existedUser.email, userId: existedUser.id } }, jwtSecret, { expiresIn: jwtTime }),
            data: userMap(existedUser)
        };
    }
}

module.exports = Create;
