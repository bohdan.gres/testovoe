const BaseValidator = require('./../../BaseValidator');

const schema = {
    email: { type: "email" },
    password: { type: "string",  min: 6, max: 16 }
};

class CreateValidator extends BaseValidator {
    constructor() {
        super(schema);
    }
}

module.exports = new CreateValidator(schema);
