const Create = require('./Create');
const Check = require('./Check');

module.exports = {
    Create,
    Check
};
