const Base = require('./../../Base');
const BaseError = require('./../../../errors/BaseError');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const Message = require('./../../../models/Message');
const { dumpMessage } = require('./../../../models/utils');

class Show extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ messageId }) {
        const { userId } = this.context;
        const existedUser = await User.findOne({ id: userId });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }

        const message = await Message.findOne({
            id: messageId
        });

        if (!message) {
            throw new BaseError({ message: 'No such message' });
        }

        return {
            Status : 1,
            data: {
                message: dumpMessage(message)
            }
        };
    }
}

module.exports = Show;
