const BaseValidator = require('./../../BaseValidator');

const schema = {
    messageId: { type: 'uuid' }
   };

class ShowValidator extends BaseValidator {
    constructor() {
        super(schema);
    }
}

module.exports = new ShowValidator();
