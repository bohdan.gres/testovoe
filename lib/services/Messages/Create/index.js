const Base = require('./../../Base');
const BaseError = require('./../../../errors/BaseError');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const Chat = require('./../../../models/Chat');
const Message = require('./../../../models/Message');
const { jwtSecret, jwtTime } = require('./../../../../etc/config');
const { dumpMessage } = require('./../../../models/utils');

class Create extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ userId, chatId, message }) {
        const existedUser = await User.findOne({ id: userId });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }

        const existedChat = await Chat.findOne({ id: chatId });

        if (!existedChat) {
            throw new BaseError({ message: 'No such chat' });
        }

        const messageInstance = await Message.create({
            userId,
            chatId,
            message
        });

        return {
            Status : 1,
            data: {
                message: dumpMessage(messageInstance)
            }
        };
    }
}

module.exports = Create;
