const Create = require('./Create');
const Show = require('./Show');
const Update = require('./Update');

module.exports = {
    Create,
    Show,
    Update
};
