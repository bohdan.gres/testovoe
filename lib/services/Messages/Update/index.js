const Base = require('./../../Base');
const BaseError = require('./../../../errors/BaseError');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const Chat = require('./../../../models/Chat');
const Message = require('./../../../models/Message');
const { jwtSecret, jwtTime } = require('./../../../../etc/config');
const { dumpMessage } = require('./../../../models/utils');

class Update extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ messageId, message }) {
        const { userId } = this.context;
        const existedUser = await User.findOne({ id: userId });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }

        const messageInstance = await Message.findOne({
            id: messageId
        });

        if (!messageInstance) {
            throw new BaseError({ message: 'No such message' });
        }
        messageInstance.message = message;
        await messageInstance.save();

        return {
            Status : 1,
            data: {
                message: dumpMessage(messageInstance)
            }
        };
    }
}

module.exports = Update;
