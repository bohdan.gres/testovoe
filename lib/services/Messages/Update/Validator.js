const BaseValidator = require('./../../BaseValidator');

const schema = {
    messageId: { type: 'uuid' },
    message: { type: 'string' }
};

class UpdateValidator extends BaseValidator {
    constructor() {
        super(schema);
    }
}

module.exports = new UpdateValidator();
