const BaseValidator = require('./../../BaseValidator');

const schema = {
    userId: { type: "uuid"},
   };

class CreateValidator extends BaseValidator {
    constructor() {
        super(schema);
    }
}

module.exports = new CreateValidator();
