const Base = require('./../../Base');
const BaseError = require('./../../../errors/BaseError');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const Chat = require('./../../../models/Chat');

class Create extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ userId }) {
        const existedUser = await User.findOne({ id: userId });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }
        const chat = await Chat.create({
            userId
        });

        return {
            Status : 1,
            data: {
                chatId: chat.id
            }
        };
    }
}

module.exports = Create;
