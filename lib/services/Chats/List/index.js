const Base = require('./../../Base');
const BaseError = require('./../../../errors/BaseError');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const Chat = require('./../../../models/Chat');
const Message = require('./../../../models/Message');

class Create extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute() {
        const { userId } = this.context;
        const existedUser = await User.findOne({ id: userId });

        if (!existedUser) {
            throw new BaseError({ message: 'No such user' });
        }
        const chats = await Chat.find({
            userId
        });

        const result = chats.map(async chat => {
           const messages = await Message.find({
               chatId: chat.id
           });
           return {
               chatId: chat.id,
               messages: messages.map(message => ({
                   id: message.id,
                   message: message.message
               }))
           }
        });
        const chatList = await Promise.all(result);

        return {
            Status : 1,
            data: {
                chatList
            }
        };
    }
}

module.exports = Create;
