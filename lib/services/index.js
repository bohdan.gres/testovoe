const Chats = require('./Chats');
const Messages = require('./Messages');
const Session = require('./Session');
const Users = require('./Users');

module.exports = {
    Chats,
    Messages,
    Session,
    Users
};
