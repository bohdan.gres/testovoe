const jwt = require('jsonwebtoken');
const { jwtSecret, jwtTime } = require('./../../../../etc/config');
const Base = require('./../../Base');
const Validator = require('./Validator');
const User = require('./../../../models/User');
const BaseError = require('./../../../errors/BaseError');
const { userMap } = require('./../../../models/utils');

class Create extends Base {
    constructor(context) {
        super(context);
        this.validator = Validator;
    }

    async execute({ email, password }) {
        const existedUser = await User.findOne({ email });
        if (existedUser) {
            throw new BaseError({ message: 'Such user already exist' });
        }
        const passwordData = await User.encryptPassword(password);
        const user = await User.create({ email, ...passwordData });
        await user.save();

        return {
            Status : 1,
            jwt: jwt.sign({ userData: { email, userId: user.id } }, jwtSecret, { expiresIn: jwtTime }),
            data: userMap(user)
        };
    }
}

module.exports = Create;
