class BaseError extends Error {
    constructor({ message }) {
        super({ message });
        this.message = message;
        this.isCustomError = true;
        this.statusCode = 200;
    }

    getmessage() {
        return this.message;
    }

    getStatusCode() {
        return this.statusCode;
    }
}

module.exports = BaseError;
