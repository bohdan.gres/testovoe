const BaseError = require('./BaseError');

class ValidationError extends BaseError {
    constructor({ message }) {
        super({ message });
        this.statusCode = 200;
    }
}

module.exports = ValidationError;
